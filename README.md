```mermaid

flowchart LR
    subgraph odido_internet[Odido internet]
        glasfiber
    end

    subgraph SFPPLUS_devices[CCR2004-1G-12S+2XS]
        glasfiber[glasfiber] <-.-> SFP01-CCR-Odido
        SFP02-CCR-KPN
        SFP03-CCR-Odido-4G
        SFP04-CCR-CCS610
        SFP05-CCR-CRS326
        SFP09-CCR-Harvester01
        SFP10-CCR-Harvester02
    end

subgraph CCS610-8P-2S+IN
SFP04-CCR-CCS610 <-.-> SFPP-CCS-CCR2004
    P1-CCS-cam01
    P2-CCS-cam02
    P3-CCS-cam03
    P4-CCS-cam04
    P5-CCS-cam05
    P6-CCS-cam06
    P7-CCS-cam07
end

subgraph CRS326-24G-2S+RM
    SFP05-CCR-CRS326 <-.-> SFP01-CRS-CCR2004
    P1-CRS
    P2-CRS
    P3-CRS
    P4-CRS
    P5-CRS
    P6-CRS
end

subgraph AX3_living
    P1-CRS <-.-> LF-vlan-djieno
    LF-vlan-guest
    LF-vlan-djieez
        
end

subgraph AX3_firstfloor
    P2-CRS <-.-> FF-vlan-djieno
    FF-vlan-guest
    FF-vlan-djieez
end

subgraph AX3_secondfloor
    P3-CRS <-.-> SF-vlan-djieno
    SF-vlan-guest
    SF-vlan-djieez
end

subgraph AX3_shed
    P4-CRS <-.-> SD-vlan-djieno-CRS
    SD-vlan-guest
    SD-vlan-djieez
end

subgraph SXT-LTE
    SFP03-CCR-Odido-4G <-.-> Odido-4G-CCR2004
end

subgraph WAP-lora
    P6-CRS <-.-> lorawan
end
```